FROM nginx:1.22.0-alpine

COPY index.html /usr/share/nginx/html

EXPOSE 80
